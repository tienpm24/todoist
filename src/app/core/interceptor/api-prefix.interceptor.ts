import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';

import { EnvironmentService } from '@todoist/shared/services/environment.service';
import { Observable } from 'rxjs/internal/Observable';

@Injectable()
export class ApiPrefixInterceptor implements HttpInterceptor {
  constructor(private envService: EnvironmentService) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (!/^(http|https):/i.test(request.url)) {
      request = request.clone({
        url: this.envService.baseUrl + request.url,
      });
    }
    return next.handle(request);
  }
}
