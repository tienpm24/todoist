const ProjectColor: { name: string; value: string }[] = [
  { name: 'Green', value: 'green' },
  { name: 'Orange', value: 'orange' },
  { name: 'Red', value: 'red' },
  { name: 'Blue', value: 'blue' },
  { name: 'Whitesmoke', value: 'whitesmoke' },
  { name: 'Purple', value: 'purple' },
  { name: 'Hot Pink', value: 'hotpink' },
];

enum ActionType {
  UPDATE = '1',
  INSERT = '2',
  DELETE = '3',
}

export const AppConfig = {
  ProjectColor,
  ActionType,
};
