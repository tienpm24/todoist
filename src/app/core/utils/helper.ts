import { HttpParams } from '@angular/common/http';
import { FormGroup } from '@angular/forms';
import findIndex from 'lodash/findIndex';

export function fmt<TObject>(text: string, myHash: TObject) {
  let key;
  // tslint:disable-next-line: forin
  for (key in myHash) {
    text = text.replace(new RegExp('\\{' + key + '\\}', 'gm'), myHash[key]);
  }
  return text;
}

export function isEmpty(args: any): boolean {
  return (
    args === null || args === undefined || args === '' || args.length === 0
  );
}

export function isNotEmpty(args: any): boolean {
  return !isEmpty(args);
}

// tslint:disable-next-line: ban-types
export function mapToHttpParamsQuery(params: Object) {
  let httpParams: HttpParams = new HttpParams();
  for (const property in params) {
    if (params.hasOwnProperty(property) && isNotEmpty(params[property])) {
      httpParams = httpParams.set(property, params[property]);
    }
  }
  return httpParams;
}

export function mapToFormData(body: object): FormData {
  const formData = new FormData();
  for (const property in body) {
    if (body.hasOwnProperty(property) && isNotEmpty(body[property])) {
      formData.append(property, body[property]);
    }
  }
  return formData;
}

export function removeTheFirstChar(args: string): string {
  if (isEmpty(args)) {
    return '';
  }
  return args.substring(1, args.length);
}

export function removeTheLastChar(args: string): string {
  if (isEmpty(args)) {
    return '';
  }
  return args.substring(0, args.length - 1);
}

export function isString(object: any): boolean {
  return typeof object === 'string';
}

export function isDecimal(value) {
  const DECIMAL_REGEX = /^\d*\.{1}\d+$/;
  return DECIMAL_REGEX.test(value);
}

export function stringToBoolean(value: string) {
  return value === 'true';
}

export function booleanToString(value: boolean) {
  return value === true ? 'true' : 'false';
}

export function deepCopyObject(source, target) {
  Object.keys(source).forEach((property) => {
    target[property] = source[property];
  });
}

export function SearchTree<T>(
  elements: T[],
  prod: string,
  children: string,
  matchingTitle: string
) {
  let result: T[] = [];
  if (elements === undefined) {
    return [];
  }
  result = elements.filter((item) => item[prod] === matchingTitle);
  if (result.length === 1) {
    return result;
  }
  result = this.searchTree(result[children], prod, children, matchingTitle);
  return result;
}

export function searchNodeTree(element, matching, field) {
  if (element[field] === matching) {
    return element;
  } else if (element[field] !== null) {
    let i = null;
    let result = null;
    if (element.childs) {
      for (i = 0; result === null && i < element.childs.length; i++) {
        result = searchNodeTree(element.childs[i], matching, field);
      }
    }
    return result;
  }
  return null;
}

export function searchTreeByText(matching: string, tree, field1, field2) {
  const matchedList = [];
  const indexField1 = tree[field1]
    .toLowerCase()
    .indexOf(matching.toLowerCase());
  const indexField2 = tree[field2]
    .toLowerCase()
    .indexOf(matching.toLowerCase());
  if (indexField1 > -1 || indexField2 > -1) {
    if (tree !== null) {
      matchedList.push(tree);
    }
  } else if (tree[field1] !== null || tree[field2] !== null) {
    let i = null;
    let result = null;
    if (tree.children) {
      for (i = 0; i < tree.children.length; i++) {
        result = searchTreeByText(matching, tree.children[i], field1, field2);
        if (result !== null && result.length > 0) {
          matchedList.push(...result);
        }
      }
    }
  }
  return matchedList;
}

// check element in array exist in another array
export function checkElementOf2Arr(sourceArr, targetArr) {
  return sourceArr.some((element) => targetArr.indexOf(element) !== -1);
}

export function searchTreeArrayByText(
  matching: string,
  treeArray: any[],
  field1,
  field2
) {
  const matchedList = [];
  treeArray.forEach((tree) => {
    const matchedTree = searchTreeByText(matching, tree, field1, field2);
    if (matchedTree && matchedTree.length > 0) {
      matchedList.push(...matchedTree);
    }
  });

  return matchedList;
}

export function mapTreeToDictionary<T>(
  data: T[],
  key: string,
  prod: string,
  child?: string
) {
  const dataDictionary: { [key: string]: string } = {};
  try {
    data.forEach((item) => {
      dataDictionary[item[key]] = item[prod];
      if (item[child] && item[child].length > 0) {
        item[child].forEach((childItem) => {
          dataDictionary[childItem[key]] = childItem[prod];
        });
      }
    });
  } catch {
    console.log('error...');
  }
  return dataDictionary;
}

export function searchTree<T>(
  elements: T[],
  prod: string,
  children: string,
  matchingTitle: string
) {
  let result: T[] = [];
  if (elements === undefined) {
    return [];
  }
  result = elements.filter((item) => item[prod] === matchingTitle);
  if (result.length === 1) {
    return result;
  }
  result = searchTree(result[children], prod, children, matchingTitle);
  return result;
}

/**
 * update node from tree
 */
export function replaceTree(tree, objectReplace) {
  return (
    Array.isArray(tree) &&
    tree.some((value, index, array) => {
      if (value.analytic_topic_id === objectReplace.analytic_topic_id) {
        array[index] = objectReplace;
        return true;
      }
      return replaceTree(value.childs, objectReplace);
    })
  );
}

export function mapArrayToDictionary<T>(data: T[], key: string) {
  const dataDictionary: { [key: string]: T } = {};
  data.forEach((item) => {
    dataDictionary[item[key]] = item;
  });
  return dataDictionary;
}

export function filterBy<T>(data: T[], prop: string, reversed?: boolean) {
  reversed = !reversed;
  return data.sort(
    (a, b) =>
      // tslint:disable-next-line: triple-equals
      (a[prop] == b[prop] ? 0 : a[prop] < b[prop] ? -1 : 1) *
      (reversed ? -1 : 1)
  );
}

export function compareTwoFormControl(
  controlName: string,
  matchingControlName: string
) {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];
    if (!control && !matchingControlName) {
      return;
    }
    if (matchingControl.errors && !matchingControl.errors.mustMatch) {
      return;
    }
    // set error on matchingControl if validation fails
    if (control.value !== matchingControl.value) {
      matchingControl.setErrors({ mustMatch: true });
    } else {
      matchingControl.setErrors(null);
    }
  };
}

export function getArrayNumbersFromString(args: string) {
  return args.match(/\d+/g).map((n) => parseInt(n, 10));
}

export function findIndexInCell(i, j, array) {
  return findIndex(array, (chart) => {
    return chart.x === i && chart.y === j;
  });
}

export function generateGridLayout(gridLayout: any[], cols: number) {
  const arr = [];
  const listData = [];
  const gridRow = [...Array(cols).keys()];
  gridRow.forEach((col) => {
    arr.push(col);
    const item = {
      x: gridLayout.length,
      y: col,
    };
    listData.push(item);
  });
  return { arr, listData };
}

/** Utilities for datetime */
export function convertStrToYYMMdd(str) {
  const date = new Date(str);
  const mnth = ('0' + (date.getMonth() + 1)).slice(-2);
  const day = ('0' + date.getDate()).slice(-2);
  return [date.getFullYear(), mnth, day].join('-');
}

/**
 * Utilities for image
 * */
export function generateImageUrl(host: string, fileName: string) {
  return host + '/' + fileName;
}

/* Utilities for string
 * */
export function transform(str: string): string {
  str = str.toLowerCase();
  str = str.replace(/ /g, '_');
  str = str.replace(/[àáạảãâầấậẩẫăằắặẳẵ]/g, 'a');
  str = str.replace(/[èéẹẻẽêềếệểễ]/g, 'e');
  str = str.replace(/[ìíịỉĩ]/g, 'i');
  str = str.replace(/[òóọỏõôồốộổỗơờớợởỡ]/g, 'o');
  str = str.replace(/[ùúụủũưừứựửữ]/g, 'u');
  str = str.replace(/[ỳýỵỷỹ]/g, 'y');
  str = str.replace(/đ/g, 'd');
  // Some system encode vietnamese combining accent as individual utf-8 characters
  str = str.replace(/[\u0300\u0301\u0303\u0309\u0323]/g, ''); // Huyền sắc hỏi ngã nặng
  str = str.replace(/[\u02C6\u0306\u031B]/g, ''); // Â, Ê, Ă, Ơ, Ư
  return str;
}

/*
 * Utilities for chart
 * */
export function isSameTypeDataChart(
  sourceChart: string,
  destinationChart: string
) {
  if (
    sourceChart === 'line' ||
    sourceChart === 'bar' ||
    sourceChart === 'radar'
  ) {
    return (
      destinationChart === 'line' ||
      destinationChart === 'bar' ||
      destinationChart === 'radar'
    );
  }
  if (sourceChart === 'doughnut') {
    return destinationChart === 'doughnut';
  }
  if (sourceChart === 'polarArea') {
    return destinationChart === 'polarArea';
  }
}
