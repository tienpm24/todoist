import { Component, OnInit } from '@angular/core';
import { GlobalNavigationModel } from '@todoist/shared/models/global-navigation';

@Component({
  selector: 'app-layouts',
  templateUrl: './layouts.component.html',
  styleUrls: ['./layouts.component.scss'],
})
export class LayoutsComponent implements OnInit {
  appNavigation: GlobalNavigationModel[] = [
    {
      title: 'Dashboard',
      link: '/app/dashboard',
      icons: 'dashboard',
      name: 'feed',
    },
    {
      title: 'Projects',
      link: '/app/projects',
      icons: 'folder',
      name: 'people, object, user-plus',
    },

    {
      title: 'Statistic',
      link: '/app/statistic',
      icons: 'pie_chart',
      name: 'people, object, user-plus',
    },
  ];

  constructor() {}

  ngOnInit(): void {}

  check(event) {
    console.log(event);
  }
}
