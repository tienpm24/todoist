import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutsRoutingModule } from './layouts-routing.module';
import { LayoutsComponent } from './pages/layouts/layouts.component';
import { MatListModule } from '@angular/material/list';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { HeaderModule } from '@todoist/shared/ui-commons/header/header.module';
import { MenuSidenavModule } from '@todoist/shared/ui-commons/menu-sidenav/menu-sidenav.module';
import { UserProfileModule } from '@todoist/shared/ui-commons/user-profile/user-profile.module';
import { SharedModule } from '@todoist/shared/shared.module';

@NgModule({
  declarations: [LayoutsComponent],
  imports: [
    CommonModule,
    LayoutsRoutingModule,
    MatSidenavModule,
    MatListModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    HeaderModule,
    MenuSidenavModule,
    UserProfileModule,
    SharedModule,
  ],
})
export class LayoutsModule {}
