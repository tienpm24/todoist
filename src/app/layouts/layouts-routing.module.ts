import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutsComponent } from './pages/layouts/layouts.component';

const routes: Routes = [
  {
    path: 'app',
    component: LayoutsComponent,
    children: [
      {
        path: 'dashboard',
        loadChildren: () =>
          import('@todoist/modules/dashboard/dashboard.module').then(
            (m) => m.DashboardModule
          ),
      },

      {
        path: 'projects',
        loadChildren: () =>
          import('@todoist/modules/projects/projects.module').then(
            (m) => m.ProjectsModule
          ),
      },

      {
        path: 'statistic',
        loadChildren: () =>
          import('@todoist/modules/statistic/statistic.module').then(
            (m) => m.StatisticModule
          ),
      },

      {
        path: '',
        redirectTo: '/app/dashboard',
        pathMatch: 'full',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LayoutsRoutingModule {}
