import {
  AnimationTriggerMetadata,
  state,
  query,
  stagger,
} from '@angular/animations';
import { animate, style, transition, trigger } from '@angular/animations';

export const slideInOut: AnimationTriggerMetadata = trigger('slideInOut', [
  transition(':enter', [
    animate('0.5s 300ms ease-in'),
    state('create', style({ transform: 'translateX(-100%)' })),
  ]),
  transition(':leave', [
    animate('0.3s ease-out'),
    state('delete', style({ transform: 'translateX(100%)' })),
  ]),
]);
