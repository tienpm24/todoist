import { Injectable } from '@angular/core';
import { ProjectsModel, TasksModel } from '../models/projects.model';
import {
  AngularFirestore,
  AngularFirestoreCollection,
} from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root',
})
export class ProjectsService {
  private projectDoc = 'projects';
  private taskDoc = 'tasks';

  projectsRef: AngularFirestoreCollection<ProjectsModel> = null;
  tasksRef: AngularFirestoreCollection<TasksModel> = null;
  constructor(private firestore: AngularFirestore) {
    this.projectsRef = firestore.collection(this.projectDoc);
  }

  getUserProjects(): AngularFirestoreCollection<ProjectsModel> {
    return this.projectsRef;
  }

  createUserProject(data: ProjectsModel) {
    return this.projectsRef.add({ ...data });
  }

  deleteUserProject(id: string) {
    return this.projectsRef.doc(id).delete();
  }

  editUserProject(
    id: string,
    body: {
      project_name: string;
      project_color: string;
    }
  ): Promise<void> {
    return this.projectsRef.doc(id).update(body);
  }

  getUserTasks(projectId: string): AngularFirestoreCollection<TasksModel> {
    return this.projectsRef.doc(projectId).collection(this.taskDoc);
  }

  createUserTask(projectId: string, data: TasksModel) {
    return this.projectsRef
      .doc(projectId)
      .collection(this.taskDoc)
      .add({ ...data });
  }

  editUserTask(
    projectId: string,
    taskId: string,
    body: {
      task_name: string;
    }
  ) {
    return this.projectsRef
      .doc(projectId)
      .collection(this.taskDoc)
      .doc(taskId)
      .update(body);
  }

  deleteUserTasks(projectId: string, taskId: string) {
    return this.projectsRef
      .doc(projectId)
      .collection(this.taskDoc)
      .doc(taskId)
      .delete();
  }
}
