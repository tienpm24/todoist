import {
  HttpClient,
  HttpHeaders,
  HttpParams,
  HttpResponse,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { EnvironmentService } from '@todoist/shared/services/environment.service';

@Injectable({
  providedIn: 'root',
})
export class ApiDataService {
  private readonly httpHeaders = new HttpHeaders();
  private readonly httpOptions = {};

  constructor(
    private httpClient: HttpClient,
    private envService: EnvironmentService
  ) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json; charset=utf-8',
        'Access-Control-Allow-Origin': '*',
        'Cache-Control': 'no-cache',
      }),
    };
    this.httpHeaders = new HttpHeaders(this.httpOptions);
  }

  setBaseUrl(url: string) {
    this.envService.setUrl = url;
  }

  get(uri: string, params?: HttpParams) {
    return this.httpClient
      .get(uri, { headers: this.httpHeaders, params })
      .pipe(map(this.extractData));
  }

  // api post method
  post(uri: string, data?: any, params?: HttpParams) {
    return this.httpClient
      .post(uri, data, {
        headers: this.httpHeaders,
        params,
      })
      .pipe(map(this.extractData));
  }

  // api post method form-data
  postFormData(uri: string, data?: any, params?: HttpParams) {
    return this.httpClient
      .post(uri, data, {
        params,
      })
      .pipe(map(this.extractData));
  }

  // api post method form-data
  putFormData(uri: string, data?: any, params?: HttpParams) {
    return this.httpClient
      .put(uri, data, {
        params,
      })
      .pipe(map(this.extractData));
  }

  // api put method
  put(uri: string, data?: any, params?: HttpParams) {
    return this.httpClient
      .put(uri, data, {
        headers: this.httpHeaders,
        params,
      })
      .pipe(map(this.extractData));
  }

  // put blob
  putDownloadFile(uri: string, data?: any, params?: HttpParams) {
    return this.httpClient.put(uri, data, {
      headers: this.httpHeaders,
      responseType: 'blob' as 'json',
      params,
    });
  }

  // get blob
  getDownLoadFile(uri: string, params?: HttpParams) {
    return this.httpClient.get(uri, {
      headers: this.httpHeaders,
      responseType: 'blob' as 'json',
      params,
    });
  }

  // api delete method
  delete(uri: string, params?: HttpParams) {
    return this.httpClient
      .delete(`${uri}`, {
        headers: this.httpHeaders,
        params,
      })
      .pipe(map(this.extractData));
  }

  private extractData(res: HttpResponse<object>) {
    const body = res;
    return body || {};
  }
}
