import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';

@Injectable({
  providedIn: 'root',
})
export class EnvironmentService {
  private BASE_URL: string;

  constructor() {
    this.BASE_URL = environment.baseUrl;
  }

  get baseUrl() {
    return this.BASE_URL;
  }

  set setUrl(url: string) {
    this.BASE_URL = url;
  }
}
