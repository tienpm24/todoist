import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ProjectsModel } from '@todoist/shared/models/projects.model';
import { slideInOut } from '@todoist/shared/animations';

@Component({
  selector: 'app-user-projects',
  templateUrl: './user-projects.component.html',
  styleUrls: ['./user-projects.component.scss'],
  animations: [slideInOut],
})
export class UserProjectsComponent {
  selectedIndex = 'string';
  @Input() projects: ProjectsModel[] = [];

  @Output() clickProject: EventEmitter<any> = new EventEmitter();
  @Output() clickCreateProject: EventEmitter<any> = new EventEmitter();
  @Output() clickEditProject: EventEmitter<any> = new EventEmitter();
  @Output() clickDeleteProject: EventEmitter<any> = new EventEmitter();
  constructor() {}

  onClickProject(project: ProjectsModel) {
    this.selectedIndex = project.id;
    this.clickProject.emit(project);
  }

  onClickCreateProject(event) {
    setTimeout(() => {
      this.clickCreateProject.emit(event);
    }, 300);
  }

  onClickEditProject(project: ProjectsModel) {
    this.clickEditProject.emit(project);
  }

  onClickDeleteProject(projectId: string) {
    setTimeout(() => {
      this.clickDeleteProject.emit(projectId);
    }, 300);
  }
}
