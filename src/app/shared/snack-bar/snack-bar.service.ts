import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { MatSnackbarType, MatSnackbarData } from '../models/snack-bar';
import { SnackBarComponent } from './snack-bar.component';

@Injectable({
  providedIn: 'root',
})
export class SnackBarService {
  readonly type: MatSnackbarType = {
    information: 'information',
    success: 'success',
    warning: 'warning',
    error: 'error',
  };

  constructor(private snackBarService: MatSnackBar) {}

  showInfo(message?: string) {
    return this.open(message, this.type.information);
  }

  showSuccess(message?: string) {
    return this.open(message, this.type.success);
  }

  showError(message?: string) {
    return this.open(message, this.type.error);
  }

  showWarning(message?: string) {
    return this.open(message, this.type.warning);
  }

  private open(message: string, messageType = '') {
    const matSnackData: MatSnackbarData = {
      content: message,
      type: messageType,
    };
    const configSuccess: MatSnackBarConfig = {
      duration: 5000,
      horizontalPosition: 'right',
      verticalPosition: 'top',
      panelClass: ['snack-bar-message', this.type.information],
    };

    if (messageType) {
      configSuccess.panelClass = ['snack-bar-message', messageType];
    }

    return this.snackBarService
      .openFromComponent(SnackBarComponent, {
        data: matSnackData,
        ...configSuccess,
      })
      .afterDismissed();
  }
}
