import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { SnackBarService } from './snack-bar.service';
import { SnackBarComponent } from './snack-bar.component';

@NgModule({
  imports: [CommonModule, MatSnackBarModule],
  providers: [SnackBarService],
  declarations: [SnackBarComponent],
})
export class SnackBarModule {}
