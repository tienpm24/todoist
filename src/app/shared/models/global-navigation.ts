export interface GlobalNavigationModel {
  title: string;
  link: string;
  icons: string;
  name: string;
  target?: string;
}
