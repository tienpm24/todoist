export interface ProjectsModel {
  id: string;
  project_name: string;
  project_color: string;
  num_of_task?: string;
}

export interface TasksModel {
  id: string;
  task_name: string;
}
