import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TwoColumnLayoutsComponent } from './two-column-layouts.component';

describe('TwoColumnLayoutsComponent', () => {
  let component: TwoColumnLayoutsComponent;
  let fixture: ComponentFixture<TwoColumnLayoutsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TwoColumnLayoutsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TwoColumnLayoutsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
