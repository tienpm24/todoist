import { NgModule } from '@angular/core';
import { MatToolbarModule } from '@angular/material/toolbar';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatButtonModule } from '@angular/material/button';
import { MenuSidenavComponent } from './menu-sidenav.component';

@NgModule({
  declarations: [MenuSidenavComponent],
  imports: [
    CommonModule,
    CommonModule,
    MatToolbarModule,
    RouterModule,
    MatListModule,
    MatIconModule,
    MatMenuModule,
    MatSidenavModule,
    MatButtonModule,
  ],
  exports: [MenuSidenavComponent],
})
export class MenuSidenavModule {}
