import { Component, OnInit, Input } from '@angular/core';
import { GlobalNavigationModel } from '@todoist/shared/models/global-navigation';

@Component({
  selector: 'app-menu-sidenav',
  templateUrl: './menu-sidenav.component.html',
  styleUrls: ['./menu-sidenav.component.scss'],
})
export class MenuSidenavComponent implements OnInit {
  @Input() menuNavigation: GlobalNavigationModel[];
  constructor() {}

  ngOnInit(): void {}
}
