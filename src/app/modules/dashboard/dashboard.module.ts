import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// app module
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './pages/dashboard/dashboard.component';

// material module
import { MatButtonModule } from '@angular/material/button';
import { SharedModule } from '@todoist/shared/shared.module';

@NgModule({
  declarations: [DashboardComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    MatButtonModule,
    SharedModule,
  ],
})
export class DashboardModule {}
