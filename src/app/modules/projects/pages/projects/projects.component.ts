import { Component, OnInit, OnDestroy, Injector } from '@angular/core';
import {
  ProjectsModel,
  TasksModel,
} from '@todoist/shared/models/projects.model';
import { ProjectsService } from '@todoist/shared/services/projects.service';
import { takeUntil, catchError, map } from 'rxjs/operators';
import { Subject, of } from 'rxjs';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { DialogUserProjectsComponent } from '../../components/dialog-user-projects/dialog-user-projects.component';
import { SnackBarService } from '@todoist/shared/snack-bar/snack-bar.service';
import { DialogUserTasksComponent } from '../../components/dialog-user-tasks/dialog-user-tasks.component';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss'],
})
export class ProjectsComponent implements OnInit, OnDestroy {
  projectName: string;
  projectId: string;
  userProjects: ProjectsModel[];
  userTasks: TasksModel[];

  private destroyed$ = new Subject();
  constructor(
    private projectsServices: ProjectsService,
    private snackBarService: SnackBarService,
    private dialog: MatDialog
  ) {}

  private static catchError(err) {
    return of(err);
  }

  ngOnInit(): void {
    this.loadUserProjects();
  }

  ngOnDestroy() {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  onClickCreateProject(event) {
    const dialogRef = this.dialog.open(DialogUserProjectsComponent, {
      data: {
        title: `Create Project`,
        actionType: '2',
      },
    });
    const submitForm$ = dialogRef.componentInstance.submitForm.subscribe(
      (res) => {
        this.projectsServices.createUserProject(res).catch(this.catchError);
        this.loadUserProjects();
        this.snackBarService.showSuccess('Create project success');
      }
    );
    dialogRef.afterClosed().subscribe(() => {
      submitForm$.unsubscribe();
    });
  }

  onClickEditProject(project: ProjectsModel) {
    const dialogRef = this.dialog.open(DialogUserProjectsComponent, {
      data: {
        title: `Edit ${project.project_name}`,
        node: project,
        actionType: '1',
      },
    });

    const submitForm$ = dialogRef.componentInstance.submitForm.subscribe(
      (res) => {
        this.editUserProject(project.id, res);
        this.loadUserProjects();
        this.snackBarService.showSuccess('Edit project success');
      }
    );
    dialogRef.afterClosed().subscribe(() => {
      submitForm$.unsubscribe();
    });
  }

  onClickDeleteProject(projectId: string) {
    this.deleteUserProject(projectId);
    this.loadUserProjects();
    this.snackBarService.showSuccess('Delete project success');
  }

  onClickCreateTask(event) {
    if (event) {
      const dialogRef = this.dialog.open(DialogUserTasksComponent, {
        data: {
          title: `Create Task`,
          actionType: '2',
        },
      });
      const submitForm$ = dialogRef.componentInstance.submitForm.subscribe(
        (res) => {
          this.projectsServices
            .createUserTask(this.projectId, res)
            .catch(this.catchError);
          this.loadUserTask();
          this.snackBarService.showSuccess('Create task success');
        }
      );
      dialogRef.afterClosed().subscribe(() => {
        submitForm$.unsubscribe();
      });
    }
  }

  onClickEditTask(task: TasksModel) {
    const dialogRef = this.dialog.open(DialogUserTasksComponent, {
      data: {
        title: `Edit ${task.task_name}`,
        node: task,
        actionType: '1',
      },
    });

    const submitForm$ = dialogRef.componentInstance.submitForm.subscribe(
      (res) => {
        this.editUserTask(this.projectId, task.id, res);
        this.loadUserTask();
        this.snackBarService.showSuccess('Edit Task success');
      }
    );
    dialogRef.afterClosed().subscribe(() => {
      submitForm$.unsubscribe();
    });
  }

  onClickProject(data: ProjectsModel) {
    this.projectName = data.project_name;
    this.projectId = data.id;
    this.loadUserTask();
  }

  onCheckedTask(taskId: string) {
    this.deleteUserTask(this.projectId, taskId);
    this.loadUserTask();
  }

  private getUserProject() {
    return this.projectsServices
      .getUserProjects()
      .snapshotChanges()
      .pipe(
        map((change) =>
          change.map((result) => ({
            id: result.payload.doc.id,
            ...result.payload.doc.data(),
          }))
        ),
        takeUntil(this.destroyed$),
        catchError(this.catchError)
      );
  }

  private loadUserProjects() {
    this.getUserProject().subscribe((res) => {
      this.userProjects = res;
    });
  }

  private deleteUserProject(id: string) {
    return this.projectsServices.deleteUserProject(id).catch(this.catchError);
  }

  private editUserProject(
    id: string,
    data: {
      project_name: string;
      project_color: string;
    }
  ) {
    return this.projectsServices
      .editUserProject(id, data)
      .catch(this.catchError);
  }

  private getUserTasks(id: string) {
    return this.projectsServices
      .getUserTasks(id)
      .snapshotChanges()
      .pipe(
        map((change) =>
          change.map((res) => ({
            id: res.payload.doc.id,
            ...res.payload.doc.data(),
          }))
        ),
        takeUntil(this.destroyed$),
        catchError(this.catchError)
      );
  }

  private loadUserTask() {
    this.getUserTasks(this.projectId).subscribe((res) => {
      this.userTasks = res;
    });
  }

  private editUserTask(
    projectId: string,
    taskId: string,
    data: { task_name: string }
  ) {
    return this.projectsServices
      .editUserTask(projectId, taskId, data)
      .catch(this.catchError);
  }

  private deleteUserTask(projectId: string, taskId: string) {
    return this.projectsServices
      .deleteUserTasks(projectId, taskId)
      .catch(this.catchError);
  }

  private catchError(err) {
    return of(null);
  }
}
