import { Component, OnInit, Inject, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AppConfig } from '@todoist/core/configs/app-config';

@Component({
  selector: 'app-dialog-user-tasks',
  templateUrl: './dialog-user-tasks.component.html',
  styleUrls: ['./dialog-user-tasks.component.scss'],
})
export class DialogUserTasksComponent implements OnInit {
  tasksForm: FormGroup;
  floatLabel: string;
  @Output() submitForm: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA)
    public data: { title: string; node: any; actionType?: any }
  ) {
    this.initialForm();
  }

  ngOnInit(): void {
    if (this.data.actionType === AppConfig.ActionType.UPDATE) {
      this.setValues(this.data.node);
    }
  }

  private setValues(node: { id: string; task_name: string }) {
    this.tasksForm.patchValue({
      id: node.id,
      task_name: node.task_name,
    });
  }

  onSubmitForm() {
    this.submitForm.emit({
      task_name: this.tasksForm.controls.task_name.value,
    });
  }

  private initialForm() {
    this.tasksForm = this.formBuilder.group({
      task_name: [''],
    });
  }
}
