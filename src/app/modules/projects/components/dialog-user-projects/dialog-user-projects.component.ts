import { Component, OnInit, Inject, Output, EventEmitter } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AppConfig } from '@todoist/core/configs/app-config';
@Component({
  selector: 'app-dialog-user-projects',
  templateUrl: './dialog-user-projects.component.html',
  styleUrls: ['./dialog-user-projects.component.scss'],
})
export class DialogUserProjectsComponent implements OnInit {
  projectsForm: FormGroup;
  floatLabel: string;
  selectedColor: string;
  projectColor = AppConfig.ProjectColor;
  @Output() submitForm: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA)
    public data: { title: string; node: any; actionType?: any }
  ) {
    this.initialForm();
  }

  ngOnInit(): void {
    if (this.data.actionType === AppConfig.ActionType.UPDATE) {
      this.selectedColor = this.data.node.project_color;
      this.setValues(this.data.node);
    }
  }

  private setValues(node: {
    id: string;
    project_name: string;
    project_color: string;
  }) {
    this.projectsForm.patchValue({
      id: node.id,
      project_name: node.project_name,
      project_color: node.project_color,
    });
  }

  onSubmitForm() {
    this.submitForm.emit({
      project_name: this.projectsForm.controls.project_name.value,
      project_color: this.projectsForm.controls.project_color.value,
    });
  }

  private initialForm() {
    this.projectsForm = this.formBuilder.group({
      project_name: [''],
      project_color: [''],
    });
  }
}
