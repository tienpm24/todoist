import {
  Component,
  OnInit,
  Input,
  OnChanges,
  Output,
  EventEmitter,
} from '@angular/core';
import { TasksModel } from '@todoist/shared/models/projects.model';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
@Component({
  selector: 'app-user-tasks',
  templateUrl: './user-tasks.component.html',
  styleUrls: ['./user-tasks.component.scss'],
})
export class UserTasksComponent {
  @Input() projectName: string;
  @Input() userTasks: TasksModel[];
  @Output() checkedTask: EventEmitter<any> = new EventEmitter();
  @Output() clickCreateTask: EventEmitter<any> = new EventEmitter();
  @Output() clickEditTask: EventEmitter<any> = new EventEmitter();

  constructor() {}

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.userTasks, event.previousIndex, event.currentIndex);
  }

  onClickCreateTask(event) {
    this.clickCreateTask.emit(event);
  }

  onClickEditTask(task: TasksModel) {
    this.clickEditTask.emit(task);
  }

  onCheckedTask(taskId: string) {
    this.checkedTask.emit(taskId);
  }
}
