import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProjectsRoutingModule } from './projects-routing.module';
import { ProjectsComponent } from './pages/projects/projects.component';
import { SharedModule } from '@todoist/shared/shared.module';

import { UserTasksComponent } from './components/user-tasks/user-tasks.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { DialogUserProjectsComponent } from './components/dialog-user-projects/dialog-user-projects.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { SnackBarModule } from '@todoist/shared/snack-bar/snack-bar.module';
import { DialogUserTasksComponent } from './components/dialog-user-tasks/dialog-user-tasks.component';

@NgModule({
  declarations: [
    ProjectsComponent,
    UserTasksComponent,
    DialogUserProjectsComponent,
    DialogUserTasksComponent,
  ],
  imports: [
    CommonModule,
    ProjectsRoutingModule,
    SharedModule,
    MatListModule,
    MatIconModule,
    MatCheckboxModule,
    MatCardModule,
    MatButtonModule,
    MatDialogModule,
    DragDropModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatSelectModule,
    SnackBarModule,
  ],
  entryComponents: [DialogUserProjectsComponent, DialogUserTasksComponent],
})
export class ProjectsModule {}
